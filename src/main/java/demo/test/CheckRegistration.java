package demo.test;
import org.junit.BeforeClass;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.*;

public class CheckRegistration {
		
		WebDriver driver = new ChromeDriver();
	
		@BeforeClass
		public static void setup() {
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		}
		
	  	@Test
	  	public void verifyRegistrationSuccessful() {
       
	  	// Step #1: Go to site https://demoqa.com/registration/
	  	driver.get("https://demoqa.com/registration/");
		driver.manage().window().maximize();
		
		// Step #2 Click Submit button
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		
		// Step #3  Verify that "* This field is required" error message appear under
		//First Name and Last Name field, Hobby, Phone Number, Username,
		//Email, Password and Confirm Password field.
		int size = driver.findElements(By.cssSelector(".legend.error")).size();
		int number = 0 ;
		while (number < size) {
			driver.findElements(By.cssSelector(".legend.error")).get(number).getText().equals("* This field is required");
			number++;
		}
		
		// Step #4 Enter First Name
		driver.findElement(By.id("name_3_firstname")).sendKeys("Joe");
		
		// Step #5 Enter Last Name
		driver.findElement(By.id("name_3_lastname")).sendKeys("Ingles");
		
		// Step #6 Enter Hobby
		driver.findElements(By.cssSelector(".input_fields.radio_fields")).get(1).click(); //Status
		driver.findElement(By.xpath("//input[@value='dance']")).click();
		driver.findElement(By.xpath("//input[@value='reading']")).click();
		
		// Step #7 Select Country
		Select countrySelect = new Select(driver.findElement(By.id("dropdown_7")));
		countrySelect.selectByVisibleText("Mexico");
		
		// Step #8 Select Date of Birth
		Select monthSelect = new Select(driver.findElement(By.id("mm_date_8")));
		monthSelect.selectByVisibleText("12");
		Select daySelect = new Select(driver.findElement(By.id("dd_date_8")));
		daySelect.selectByVisibleText("25");
		Select yearSelect = new Select(driver.findElement(By.id("yy_date_8")));
		yearSelect.selectByVisibleText("1993");
		
		// Step #9 Enter Phone Number
		driver.findElement(By.id("phone_9")).sendKeys("0987654321");
		
		// Step #10 Enter Username
		driver.findElement(By.id("username")).sendKeys("fad1fa3d2298Sfsa34344");
		
		// Step #11 Enter Email
		driver.findElement(By.id("email_1")).sendKeys("dummy@123assew2er1d3dsfsgf.com");
		
		// Step #12 Enter Password
		driver.findElement(By.id("password_2")).sendKeys("@Password1234~");
		
		// Step #13 Enter Confirm Password
		driver.findElement(By.id("confirm_password_password_2")).sendKeys("@Password1234~");
		
		// Step #14 Click Submit button
		driver.findElement(By.name("pie_submit")).click();
		driver.findElement(By.name("pie_submit")).click();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Step #15 "Thank you for your registration" will appear
		driver.findElement(By.cssSelector(".entry-content p.piereg_message")).getText().equals("Thank you for your registration");
  }
  
  @Test
  public void verifyRegistrationUnsuccessful() {
       
	  	// Step #1: Go to site https://demoqa.com/registration/
	  	driver.get("https://demoqa.com/registration/");
		driver.manage().window().maximize();
		
		// Step #2 Click Submit button
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		
		// Step #3  Verify that "* This field is required" error message appear under
		//First Name and Last Name field, Hobby, Phone Number, Username,
		//Email, Password and Confirm Password field.
		int size = driver.findElements(By.cssSelector(".legend.error")).size();
		int number = 0 ;
		while (number < size) {
			driver.findElements(By.cssSelector(".legend.error")).get(number).getText().equals("* This field is required");
			number++;
		}
		
		// Step #4 Enter First Name
		driver.findElement(By.id("name_3_firstname")).sendKeys("Joe");
		
		// Step #5 Enter Last Name
		driver.findElement(By.id("name_3_lastname")).sendKeys("Ingles");
		
		// Step #6 Enter Hobby
		driver.findElements(By.cssSelector(".input_fields.radio_fields")).get(1).click(); //Status
		driver.findElement(By.xpath("//input[@value='dance']")).click();
		driver.findElement(By.xpath("//input[@value='reading']")).click();
		
		// Step #7 Select Country
		Select countrySelect = new Select(driver.findElement(By.id("dropdown_7")));
		countrySelect.selectByVisibleText("Mexico");
		
		// Step #8 Select Date of Birth
		Select monthSelect = new Select(driver.findElement(By.id("mm_date_8")));
		monthSelect.selectByVisibleText("12");
		Select daySelect = new Select(driver.findElement(By.id("dd_date_8")));
		daySelect.selectByVisibleText("25");
		Select yearSelect = new Select(driver.findElement(By.id("yy_date_8")));
		yearSelect.selectByVisibleText("1993");
		
		// Step #9 Enter Phone Number
		driver.findElement(By.id("phone_9")).sendKeys("0987654321");
		
		// Step #10 Enter Username
		driver.findElement(By.id("username")).sendKeys("fad1fa3d2298Sfsa34344");
		
		// Step #11 Enter Email
		driver.findElement(By.id("email_1")).sendKeys("dummy@123assew2er1d3dsfsgf.com");
		
		// Step #12 Enter Password
		driver.findElement(By.id("password_2")).sendKeys("@Password1234~");
		
		// Step #13 Enter Confirm Password
		driver.findElement(By.id("confirm_password_password_2")).sendKeys("@Password1234~");
		
		// Step #14 Click Submit button
		driver.findElement(By.name("pie_submit")).click();
		driver.findElement(By.name("pie_submit")).click();
		
		// Step #15 Error: Username already exists
		driver.findElement(By.cssSelector(".entry-content p.piereg_login_error")).getText().equals("Error: Username already exists");
		
		driver.close();
  }
}